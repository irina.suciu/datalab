package fr.datalab;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

public class App {
    /*private static final String HOST = "kafka.e4db74d0580e4c3fa1f5.centralus.aksapp.io";
    private static final int PORT = 9092;
    private static final String CHECKPOINT_DIR = "/tmp";
    //private static final Duration BATCH_DURATION = Durations.seconds(2000);


    private static final Pattern SPACE = Pattern.compile(" ");

    public static void main(String[] args) throws InterruptedException, IOException {

        // Configure and initialize the SparkStreamingContext
       // SparkConf sparkConf = new SparkConf().setMaster("local[*]").setAppName("VlbStreamingApp");
        //JavaStreamingContext streamingContext = new JavaStreamingContext(sparkConf, new Duration(1000));
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("VlbStreamingApp");
        JavaStreamingContext streamingContext =
                new JavaStreamingContext(conf, new Duration(1000));
        Logger.getRootLogger().setLevel(Level.ERROR);
        streamingContext.checkpoint(CHECKPOINT_DIR);

        //int numThreads = Integer.parseInt(args[3]);
        Map<String, Integer> topicMap = new HashMap<String, Integer>();
        topicMap.put("vlb", 1);
            /*String[] topics = args[2].split(",");
            for (String topic: topics) {
                topicMap.put(topic, numThreads);
            }*/

/*
        JavaPairReceiverInputDStream<String, String> kafkaStream =
                KafkaUtils.createStream(streamingContext,HOST+":"+PORT,"kafka-group1", topicMap);

        JavaDStream<String> lines = kafkaStream.map(tuple2 -> tuple2._2());
        JavaDStream<String> jsonStream = lines.flatMap(new FlatMapFunction<String, String>() {
            public Iterator<String> call(String x)  {
                try {
                    JsonObject jsonObject =  new JsonParser().parse(x).getAsJsonObject();
                    // read from the json object the value of the attribute "text" - in a tweet this is the current text of the tweet
                    String textline=jsonObject.getAsString();
                    // split into words
                    return (Iterator<String>) Lists.newArrayList(textline);

                } catch (Exception e) {
                    // sometimes the server does not transfer the correct result due to special characters, we ignore them

                }
                return (Iterator<String>) Lists.newArrayList("");
            }
        });


       // jsonStream.print();

/*
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http"),
                        new HttpHost("localhost", 9201, "http")));

        JavaEsSparkStreaming.saveJsonToEs(jsonStream,"spark/docs");
*/
        //streamingContext.start();
        //streamingContext.awaitTermination();

        //client.close();
    //}
}
