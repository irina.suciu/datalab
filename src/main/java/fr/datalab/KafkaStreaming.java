package fr.datalab;

import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.net.InetAddress;
import java.security.Timestamp;
import java.util.*;


public class KafkaStreaming {

    private final static String TOPIC = "vlb";
    private final static String BOOTSTRAP_SERVERS =
            "kafka.e4db74d0580e4c3fa1f5.centralus.aksapp.io:9092";

        public static void main(final String[] args) throws Exception {
            runConsumer();
        }
    private static Consumer<Long, String> createConsumer() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG,
                "Group-1");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());
        //props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "PLAINTEXT");

        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put("security.protocol", "SASL_PLAINTEXT");
        props.put("sasl.mechanism", "PLAIN");
        props.put("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"test\" password=\"test123\";");

        // Create the consumer using props.
        final Consumer<Long, String> consumer =
                new KafkaConsumer<>(props);
        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList(TOPIC));

        return consumer;
    }
    static void runConsumer() throws InterruptedException, IOException {
        final Consumer<Long, String> consumer = createConsumer();
        final int giveUp = 100;
        int noRecordsCount = 0;

        while (true) {
            final ConsumerRecords<Long, String> consumerRecords =
                    consumer.poll(3000);

            if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            // RESTCLient for ELS
            RestHighLevelClient client = new RestHighLevelClient(
                    RestClient.builder(
                            new HttpHost("elastic.e4db74d0580e4c3fa1f5.centralus.aksapp.io", 9200, "http")));

            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("contract_name", "marseille");
            jsonMap.put("name", "9087-MAZARGUES");
            jsonMap.put("address", "trying out Elasticsearch");
            IndexRequest indexRequest = new IndexRequest("vlb")
                    .id("1").source(jsonMap);
            try {
                IndexResponse response = client.index(indexRequest, RequestOptions.DEFAULT);
                System.out.println(response.toString());
            } catch(ElasticsearchException e) {
                System.out.println("NONONO");
                e.printStackTrace();
                if (e.status() == RestStatus.CONFLICT) {

                }
            }
            consumerRecords.forEach(record -> {
               /* System.out.printf("Consumer Record:(%d, %s, %d, %d)\n",
                        record.key(), record.value(),
                        record.partition(), record.offset());*/

                JSONParser jsonParser = new JSONParser();
                Object obj = null;
                try {
                    obj = jsonParser.parse(record.value());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                JSONArray vlibList = (JSONArray) obj;
                System.out.println(vlibList);

                //Iterate over vlbObject array
                vlibList.forEach( vlb -> parseVlbObject( (JSONObject) vlb ) );



            });
            //consumer.commitAsync();
            client.close();
        }
        consumer.close();

        System.out.println("DONE");
    }
    private static void parseVlbObject(JSONObject vlbObject)
    {

        Long number = (Long) vlbObject.get("number");
        String contract_name = (String) vlbObject.get("contract_name");
        String name = (String) vlbObject.get("name");
        String address = (String) vlbObject.get("address");
        JSONObject position = (JSONObject) vlbObject.get("position");
        Double lat = (Double) position.get("lat");
        Double lng = (Double) position.get("lng");

        Boolean banking = (Boolean) vlbObject.get("banking");
        Boolean bonus = (Boolean) vlbObject.get("bonus");
        Long bike_stands = (Long) vlbObject.get("bike_stands");
        Long available_bike_stands = (Long) vlbObject.get("available_bike_stands");
        Long available_bikes = (Long) vlbObject.get("available_bikes");
        String status = (String) vlbObject.get("status");
        Long last_update = (Long) vlbObject.get("last_update");
        Date d =  last_update != null ?  new Date(last_update) : null;

    }

}
